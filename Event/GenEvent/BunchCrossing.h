#ifndef GENEVENT_BUNCHCROSSING_H
#define GENEVENT_BUNCHCROSSING_H

/** @class gen::BunchCrossing GenEvent/GenEvent/BunchCrossing.h BunchCrossing.h
 *
 *  ...
 *
 *  @author J. Lingemann
 *  @date 09/05/2016
 */

#include "HepMC/GenEvent.h"
#include "GaudiKernel/KeyedObject.h"



namespace gen {
// class HepMCExtension {
// public:
//   HepMCExtension();
// };
// FIXME should be:
// typedef std::vector<const std::shared_ptr<HepMC::GenEvent>> HepMCEvents;
typedef std::vector<HepMC::GenEvent*> HepMCEvents;
// typedef std::vector<const std::shared_ptr<gen::HepMCExtension>> HepMCExtensions m_extensions;
class BunchCrossing: public DataObject {
public:
  /// Default constructor
  BunchCrossing() : m_signal(nullptr), m_eventNumber(-1) {};
  /// Destructor
  virtual ~BunchCrossing();

  void clearEvents();
  void removeEvent(HepMC::GenEvent* aEvent);

  // Getter functions
  long eventNumber() const {return m_eventNumber;}
  int numCollisions() const {return m_events.size();}
  const std::vector<HepMC::GenEvent *>& events() const {return m_events;}
  // for convenience:
  HepMCEvents::const_iterator begin() const {return m_events.cbegin();}
  HepMCEvents::const_iterator end() const {return m_events.cend();}

  // Setter functions
  void addEvent(HepMC::GenEvent* aEvent);
  void setEvents(std::vector<HepMC::GenEvent*>&& events) {m_events = events;}
  void setEventNumber(long aEventNumber) {m_eventNumber = aEventNumber;}
  std::vector<HepMC::GenEvent *>& events() {return m_events;}

  HepMC::GenEvent* lastEvent() {return m_events.back(); }
  HepMC::GenEvent* firstEvent() {return *m_events.begin(); }

  HepMCEvents::iterator begin() {return m_events.begin();}
  HepMCEvents::iterator end() {return m_events.end();}

  void setLastAsSignal() { m_signal = lastEvent(); }
  const HepMC::GenEvent* signal() const;

private:
  HepMCEvents m_events;         ///< all events: first in list assumed to be signal
  HepMC::GenEvent* m_signal;    ///< Pointer to the event in m_events that is the signal (usually first)
  // HepMCExtensions m_extensions; ///< any information needed in addition to HepMC
  long m_eventNumber;
};

void copyCollisions( const gen::HepMCEvents& from , gen::HepMCEvents& to );
void clearCollisions( gen::HepMCEvents& events );

}

#endif /* define GENEVENT_BUNCHCROSSING_H */
